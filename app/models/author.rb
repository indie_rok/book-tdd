class Author < ActiveRecord::Base
    validates :first_name, :last_name, presence: true
    has_many :publications
    has_many :books,through: :publications
    def full_name
        "#{first_name } #{last_name}"
    end
end