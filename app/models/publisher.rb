class Publisher < ActiveRecord::Base
  validates_presence_of :name
  validates_length_of :name, minimum: 5
  validates_length_of :name, maximum: 40
  validates_uniqueness_of :name,  case_sensitive: false
end