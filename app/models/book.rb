class Book < ActiveRecord::Base
  belongs_to :publisher
  has_many :publications
  has_many :authors, through: :publications

  validates :title, presence: true
  validates :idsn, presence: true
  validates :page_count, presence: true
  validates :price, presence: true
  validates :publisher_id, presence: true
end