class PublishersController < ApplicationController
  
  
  before_action :set_publisher, only: [:show, :edit, :update,:destroy]
  
  def index
    @publishers = Publisher.all
  end
  
  def show
    
  end
  
  def new
    
  end
  
  def create
    @publisher = Publisher.new(publisher_params)
    if @publisher.save
      flash[:success] = 'Salvado!'
      redirect_to publisher_path(@publisher)
    else
      flash[:error] = 'Dont created!'
      render :new
    end
  end
  
  def edit
    
  end
  
  def update
    @publisher.update(publisher_params)
    redirect_to @publisher
  end
  
  def destroy
    @publisher.destroy
    redirect_to publishers_path()
  end
  
  private
  
  def publisher_params
    params.require(:publisher).permit(:name)
  end
  
  def set_publisher
    @publisher = Publisher.find(params[:id])
  end
    
end