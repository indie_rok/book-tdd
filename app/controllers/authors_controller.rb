class AuthorsController < ApplicationController
  
  before_action :set_user, only: [:show, :edit,:update,:destroy]
  
  
  def index
    
  end
  
  def show
  end
  
  def new
    @author = Author.new
  end
  
  def create
    
    @author = Author.new(author_params)
    if @author.save
      flash[:success] = 'Saved!'
      redirect_to @author
    else
      flash[:error] = 'Sorry, no created'
      redirect_to new_author_path
    end
  end
  
  def edit

    if(@author)
      render :edit
    else
      flash[:error] = 'author not found'
      redirect_to authors_path
    end
    
  end
  
  def update

    if @author.update author_params
      flash[:success] = 'Author updated'
      redirect_to @author
    else
      flash[:error] = 'Invalid data'
      redirect_to authors_path
    end
    
  end
  
  def destroy
    flash[:success] = 'Deleted!'
    @author.destroy
    redirect_to authors_path
  end
  
  private
  
  def author_params
    params.require(:author).permit(:first_name,:last_name)
  end
  
  def set_user
    @author = Author.find(params[:id])
  end
  
end
