class SessionsController < ApplicationController
  
  def new

    redirect_to root_url if current_user
    
  end
  
  def create
    @user = User.find_by(email: params[:email])
    if @user and @user.authenticate(params[:password])
      session[:user_id] = @user.id
      flash[:success] = "Loged In"
      redirect_to root_path
    else
      flash[:danger] = "Not loged in"
      render :new
    end
  end
  
  def destroy
    reset_session
    redirect_to root_path
  end
  
  private
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  
end