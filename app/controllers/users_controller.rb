class UsersController < ApplicationController
  
  before_action :set_user,only: [:show]
  
  def show
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_attributes)
    
    if @user.save
      flash[:success] = "User Created"
      redirect_to @user
    else
      flash[:danger] = "User not created"
      #require "pry";binding.pry
      render :new
    end
  end
  
  private 
  def set_user
    @user = User.find(params[:id])
  end
  
  def user_attributes
    params.require(:user).permit(:first_name,:last_name,:email,:password)
  end
end