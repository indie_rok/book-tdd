# Book Fabricator
Fabricator(:book) do
title { Faker::Lorem.sentence }
price { Faker::Commerce.price }
idsn { Faker::Number.number(5)}
page_count { Faker::Number.number(4) }
publisher
end
