require 'rails_helper'

RSpec.describe UsersController, type: :controller do
 
 
 describe "#show" do
   let(:user){Fabricate(:user)}
   it "should return a succesfull http request" do
    get :show,id: user.id
    expect(response).to have_http_status(:success)
   end
 end
 
 describe "#new" do
   it "should return a succesfull http request" do
     get :new
     expect(response).to have_http_status(:success)
   end
 end
 
 describe "#create" do
  context "user create with correct parameters" do
    let(:user){Fabricate.attributes_for(:user)}
    
    it "creates the user" do
      post :create, user: user
      #
      expect(User.last.first_name).to eq(user[:first_name])
    end
    
    it "sets the flash msj" do
      post :create, user: user
      expect(flash[:success]).to eq("User Created")
    end
    
    it "redirects to user profile" do
      post :create, user: user
      #
      expect(response).to redirect_to(user_path(User.last))
    end
    
  end
  
  context "user not created with incorrect parameters" do
    let(:user){Fabricate.attributes_for(:user,first_name: nil)}
    before(:each) do
      post :create, user: user
    end
    it "does not save the record" do
      
      expect(User.count).to eq(0)
    end
    
    it "saves the error msj" do
      expect(flash[:danger]).to eq("User not created")
    end
    
  end
  
 end
 
end
