require 'rails_helper'

RSpec.describe PublishersController, type: :controller do
  describe 'get #index' do
    it 'returns a successfull htpp request' do
      get :index
      expect(response).to have_http_status(:success)
    end
  end
  
  describe 'GET #show' do
    let(:publisher) {Fabricate(:publisher)}
    it 'should  return a succesfull htpp request' do
      get :show, id: publisher.id
      expect(response).to have_http_status(:success)
    end
  end
  
  describe 'GET #new' do
    it 'should have a succesfull http reponse' do
      get :new
      expect(response).to have_http_status :success
    end
  end
  
  describe 'POST #create' do
    context 'succesfully creates de publisher' do
      let(:new_publisher) { Fabricate.build(:publisher) }
      it 'saves the record in data base' do
        post :create, publisher: {name: new_publisher.name}
        expect(Publisher.last.name).to eq(new_publisher.name)
      end
      
      it 'sets a flash confirmation msj' do
        post :create, publisher: {name: new_publisher.name}
        expect(flash[:success]).to eq('Salvado!')
      end
      
      it 'it redirects to the publisher' do
        post :create, publisher: {name: new_publisher.name}
        expect(response).to redirect_to(publisher_path(Publisher.last))
      end
    end
    
    context 'unsucesfully creates de pubisher' do
      let(:wrong_publisher){Fabricate.build(:publisher,name: nil)}
      
      it 'does not save anything in the database' do
        post :create, publisher: {name: wrong_publisher.name}
        expect(Publisher.count).to eq(0)
      end
      
      it 'sets error flash msj' do
        post :create, publisher: {name: wrong_publisher.name}
        expect(flash[:error]).to eq('Dont created!')
      end
      
      
    end
  end
  
  describe 'GET #edit' do
    it 'returns a succesfull http code' do
      publisher = Fabricate(:publisher)
      get :edit, id: publisher.id
      expect(response).to have_http_status(:success)
    end
  end
  
  describe 'PUT #update' do
    context 'succesfully edits the publisher' do
      it 'edits the publisher' do
        publisher = Fabricate(:publisher)
        put :update, id: publisher.id ,publisher: {name: 'New name'}
        expect(Publisher.last.name).to eq('New name')
        expect(Publisher.last.name).not_to eq(publisher.name)
      end
    end
    
    context 'unsuccesfully edits the publisher' do
      it 'does not save the modified info' do
        publisher = Fabricate(:publisher)
        put :update, id: publisher.id ,publisher: {name: 'aaa'}
        expect(Publisher.last.name).not_to eq('aaa')
        
      end
    end
  end
  
  describe 'DELETE #destroy' do
    it 'it destroys the record' do
      publisher = Fabricate(:publisher)
      
      delete :destroy, id: publisher.id
      expect { delete :destroy, {:id => publisher.id}}.to raise_error ActiveRecord::RecordNotFound
      
    end
  end
  
end