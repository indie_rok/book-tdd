require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  
  let(:user){Fabricate(:user)}
  
  describe '#new' do
    context 'user signed in' do
      it 'redirects user to root url' do
        session[:user_id] = user.id
        get :new
        expect(response).to redirect_to root_path
        
      end
    end
    
    context 'user not signed in' do
      it 'returns a succesfull htpp request code' do
        get :new
        expect(response).to have_http_status(:success)
      end
    end
    
  end
  
  describe '#create' do
    context 'Succesful sign in' do
    before(:each) do
      post :create, email: user.email,password: user.password
    end
    
    it 'creates the session' do
      expect(session[:user_id]).to eq(user.id)
    end
    
    it 'sets the flash msj' do
      expect(flash[:success]).to be_present
    end
    
    it 'redirects to root path' do
      expect(response).to redirect_to root_path
    end
    
    context 'Unsuccesful sign in' do
      before(:each) do
        post :create, email: user.email, password: ('a'..'z').to_a.sample(5).join
      end
      
      it 'does not create the session' do
        expect(session[:id]).to be_nil
      end
      
      it 'sets the error msg' do
        expect(flash[:danger]).to be_present
      end
    end
    end
    
  end
  
  describe '#delete' do
    
    before(:each) do
      session[:user_id] = user.id
      delete :destroy, id: user.id
    end
    
    it 'logs to user out' do
      
      expect(session[:user_id]).to be_nil
      
    end
    
    it "redirects the user" do
      expect(response).to redirect_to root_url
    end
  end
  
end