require 'rails_helper'

RSpec.describe AuthorsController, type: :controller do
   
   describe 'get #index' do
     it 'should get a succesfull http code in index' do
       get :index
       expect(response).to have_http_status :success
     end
   end
   
   describe 'get #show' do
     it 'should get a succesfull http response when accesing an author' do
       author = Fabricate(:author)
       get :show, id: author.id
       expect(response).to have_http_status :success
     end
   end
   
   describe 'get #new' do
     it 'should return a succesfull http response when creating a new author' do
      get :new
      expect(response).to have_http_status :success
     end
   end
   
   describe 'post #post' do
    context 'succesfully creates an author' do
      
      it 'saves an author object' do
        count = Author.count
        post :create, author: Fabricate.attributes_for(:author)
        expect(Author.count).to eq(count+1)
      end
      
      it 'writes the flash msj' do
        post :create, author: Fabricate.attributes_for(:author)
        expect(flash[:success]).to eq('Saved!')
      end
      
      it 'redirects to author page' do
        post :create, author: Fabricate.attributes_for(:author)
        expect(response).to redirect_to author_path(Author.last)
      end
      
    end
    
    context 'unsuccesfully creates an author' do
      it 'does not save and author object to the database' do
        count = Author.count
        post :create, author: Fabricate.attributes_for(:author,first_name: nil)
        expect(Author.count).to eq(count)
      end
      
      it 'sets the correct flash msj for errors' do
        post :create, author: Fabricate.attributes_for(:author,first_name: nil)
        expect(flash[:error]).to eq('Sorry, no created')      
      end
    end
   end
  
    
   describe 'get #edit' do
    let(:author) {Fabricate(:author)}     
     
     context 'finds to user to edit' do
        it 'should retrive the user data' do
           author = Fabricate(:author)
           get :edit, id: author.id
           expect(response).to have_http_status(:success)
      end
     end
     
     context 'does not find the user to edit' do
       
        it 'should raise and error of record not found' do
          author
          not_valid_id = Author.last.id + 1
          expect { get :edit, {:id => not_valid_id}}.to raise_error ActiveRecord::RecordNotFound
        end
        
     end
     
   end
   
   describe 'PUT #update' do
     let(:author) {Fabricate(:author)}
     
     context 'succesfully edits' do
       
       
       
       it 'changes the name of the author' do
         put :update, id: author.id, author: {first_name: 'Emmaaaa'}
         
         expect(Author.last.first_name).to eq('Emmaaaa')
         expect(Author.last.first_name).not_to eq(author.first_name)
         
       end
       
       it 'changes the last name of the author' do
         put :update, id: author.id, author: {last_name: 'Emmaaaa'}
         
         expect(Author.last.last_name).to eq('Emmaaaa')
         expect(Author.last.last_name).not_to eq(author.first_name)
       end
       
       it 'set up a confirmation flash msj' do
    
         put :update, id: author.id, author: {first_name: 'Emmaaaa'}
         expect(flash[:success]).to eq('Author updated')
         
       end
       
       it 'redirects to #show edit number' do
         put :update, id: author.id, author: {first_name: 'Emmaaaa'}
         expect(response).to redirect_to author
         
       end
       
     end
     
     context 'unsuccesfully edit' do
       
       it 'does not save the edit' do
         put :update, id: author.id, author: {first_name: nil}
         expect(Author.last.first_name).to eq(author.first_name)
       end
       
       it 'sends an error flash msj' do
         put :update, id: author.id, author: {first_name: nil}
         expect(flash[:error]).to eq('Invalid data')
       end
       
       it 'redirects to index' do 
         put :update, id: author.id, author: {first_name: nil}
         expect(response).to redirect_to authors_path
       end
       
     end
   end
   
   describe 'DELETE #destroy' do
    
     let(:author) {Fabricate(:author)}
     
     context 'destroys the record' do
       it 'should destroy the record from the database' do
         delete :destroy,id: author.id
         expect(Author.count).to eq(0)
       end
      
       it 'should flash a success msj' do
         delete :destroy,id: author.id
         expect(flash[:success]).to eq('Deleted!')
       end
       
       it 'should redirect to index' do
         delete :destroy,id: author.id
         expect(response).to redirect_to authors_path
       end
     end
     
     context 'does not destroys the record' do
       it 'does not destroys the record for invalid id' do
         invalid_id = author.id + 1
         delete :destroy,id: author.id
         expect { delete :destroy, {:id => invalid_id}}.to raise_error ActiveRecord::RecordNotFound
       end
     end
     
   end
   
end