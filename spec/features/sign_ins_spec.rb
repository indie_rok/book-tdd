require 'rails_helper'

RSpec.feature "SignIns", type: :feature do
  let(:user){Fabricate(:user)}
  scenario 'User types correct passwords' do
    
    visit root_url
    click_link "Sign in"
    fill_in "email",with: user.email
    fill_in "password", with: user.password
    click_button "Sign In"
    expect(page).to have_content("Loged In")
    
  end
  
end