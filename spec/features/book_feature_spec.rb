require 'rails_helper'

RSpec.feature 'Creating Books' do
  
  
  let(:author1){Fabricate(:author,first_name:"Emmanuel",last_name:"Orozco")}
  let(:author2){Fabricate(:author,first_name:"Alejandro",last_name:"Ferreyra")}
  
  scenario "with incorrect inputs" do
    Fabricate(:publisher, name: "Algaguara")
    visit root_path
    click_link "Books"
    click_link "Create book"
    fill_in "book_title", with: "Shibumi"
    fill_in "book_idsn", with: "1233332"
    fill_in "book_page_count", with: 233
    fill_in "book_price", with: 3.55
    select "Algaguara", from: "book_publisher_id"
    
    #check author1.full_name
    #check author2.full_name
    
    click_button "Create Book"
    
    #expect(page).to have_content("Book has not been created")
        
  end

end