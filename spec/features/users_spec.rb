require 'rails_helper'

RSpec.feature "User", type: :feature do
  scenario 'sign up is working' do
    visit root_path
    click_link "Sign Up"
    fill_in :user_first_name, with: "Emmanuel"
    fill_in :user_last_name, with: "Orozco"
    fill_in :user_email, with: "yo@emmanuelorozco.com"
    fill_in :user_password, with: "password"
    click_button "Create User"
    
    expect(page).to have_content ("User Created")
    
  end
  
  scenario 'sign up is not working' do
    visit root_path
    click_link "Sign Up"
    fill_in :user_first_name, with: nil
    fill_in :user_last_name, with: nil
    fill_in :user_email, with: nil
    fill_in :user_password, with: nil
    fill_in :user_password_confirmation, with: nil
    click_button "Create User"
    
    expect(page).to have_content ("User not created")
    
  end
  
  scenario 'can visit a user profile' do
    user = Fabricate(:user)
    visit user_path(User.last)
    
    within("h3.titles") do
      expect(page).to have_content(user[:first_name])
    end
    
    within("#first-name") do
      expect(page).to have_content(user[:first_name])
    end
    
    within("#last-name") do
      expect(page).to have_content(user[:last_name])
    end
    
    within("#email") do
      expect(page).to have_content(user[:email])
    end
    
    
  end
  
end
