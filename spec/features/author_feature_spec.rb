require 'rails_helper'

RSpec.feature 'Creating authors' do
  
  scenario 'with sucessfull response' do
    visit root_path
    click_link 'Create new author'
    fill_in 'author_first_name', with:'Emmanuel'
    fill_in 'author_last_name', with:'Orozco'
    
    click_button 'Create Author'
    
    expect(page).to have_content('Saved!')
    
  end
  
  scenario 'with unsucesfull response' do
    visit root_path
    click_link 'Create new author'
    fill_in 'author_first_name', with:nil
    fill_in 'author_last_name',with:nil
    click_button 'Create Author'
    
    expect(page).to have_content('Sorry, no created')
    
  end

end