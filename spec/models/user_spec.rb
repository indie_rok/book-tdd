require 'rails_helper'

RSpec.describe User, type: :model do
  
  it 'shouuld have first name' do
    john = User.new(first_name: nil, last_name:"Orozco",email:'coo@asd.com',password_digest:"password")
    #require 'pry';binding.pry
    expect(john).not_to be_valid
    expect(john.errors[:first_name].any?).to be_truthy
  end
  
  it 'should have last name' do
   john = User.new(first_name: "Emmanuel", last_name: nil,email:'coo@asd.com',password:"password")
    expect(john).not_to be_valid
    expect(john.errors[:last_name].any?).to be_truthy 
  end
  
  it 'should have an email' do
    john = User.new(first_name: "Emmanuel", last_name: "Orozco",email: nil,password:"password")
    expect(john).not_to be_valid
    expect(john.errors[:email].any?).to be_truthy 
  end
  
  it 'should have a password' do
    john = User.new(first_name: "Emmanuel", last_name: "Orozco" ,email:'coo@asd.com',password: nil)
    expect(john).not_to be_valid
    expect(john.errors[:password].any?).to be_truthy 
  end
  
  it 'should be a valid email address' do
    user = Fabricate.build(:user,email:'michal er@test.com')
    expect(user).not_to be_valid
    expect(user.errors[:email].any?).to be_truthy
  end
  
  describe '#fullname' do
    it 'should return a full name' do
      john = User.new(first_name: "Emmanuel", last_name: "Orozco" ,email:'coo@asd.com',password: "asd")
      expect(john.full_name).to eq("Emmanuel Orozco")
    end
  end
  
end
