require 'rails_helper'

#title
#idsn
#page_count
#price
#publisher_id

RSpec.describe Book, type: :model do
  it { should belong_to(:publisher) }
  it { should have_many(:publications) }
  
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:idsn) }
  it { should validate_presence_of(:page_count) }
  it { should validate_presence_of(:price) }
  it { should validate_presence_of(:publisher_id) }
  
  it { should have_many(:publications)}
  it { should have_many(:authors).through(:publications) }
  
end