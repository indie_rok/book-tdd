require 'rails_helper'

RSpec.describe Publisher, type: :model do
    describe ':name' do
       it 'name should not be null' do
           should validate_presence_of(:name)
       end
  
       it 'name should not be longer shorter than 15 characters' do
          should validate_length_of(:name).is_at_least(5) 
       end
       
       it 'name should not be longer longer than 40 characters' do
          should validate_length_of(:name).is_at_most(40) 
       end
  
       it 'name should be unique' do
        should validate_uniqueness_of(:name).case_insensitive
       end
       
       
       
    end
end