require 'rails_helper'

RSpec.describe Author, type: :model do
  
    
  it 'name should not be null' do
      author = Fabricate.build(:author,first_name: nil)
      expect(author).not_to be_valid
      expect(author.errors[:first_name].any?).to be_truthy
  end
  
  it 'last name should not be null' do
      author = Fabricate.build(:author,last_name: nil)
      expect(author).not_to be_valid
      expect(author.errors[:last_name].any?).to be_truthy
  end
  
  describe Author do
    it { should have_many(:publications) }
    it { should have_many(:books).through(:publications)}
  end
  
  describe '#{#fullname}' do
     it 'should return the full name' do
      author = Fabricate.build(:author,first_name:'Emmanuel',last_name:'Orozco')
      expect(author.full_name).to eq("Emmanuel Orozco")
     end
  end
  
end