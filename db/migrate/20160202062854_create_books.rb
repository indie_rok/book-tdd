class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :title
      t.string :idsn
      t.integer :page_count,:numericality => { :greater_than => 0 }
      t.decimal :price, numericality: {greater_than_or_equal_to: 0}
      t.timestamps null: false
      t.belongs_to :publisher,index: true
    end
  end
end
