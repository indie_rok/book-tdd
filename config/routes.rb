Rails.application.routes.draw do
  resources :authors
  resources :publishers
  resources :books
  resources :users, only: [:create,:show,:new,:index]
  resources :sessions, only: [:create,:new,:destroy]
  post '/signin', to: 'sessions#create', as: 'signin'
  root to: 'authors#index'
end